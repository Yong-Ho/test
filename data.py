import os
import torch
import string
import time
from  datetime import datetime
import random
#	loader.ptb
#	dataset = loader.kyp

#	dataset.train
#	--> dataset.train.inputs
#	--> dataset.train.targets 

#	dataset.valid
#	--> dataset.valid.inputs 
#	--> dataset.valid.targets 

#	dataset.idx2word 
#	dataset.word2idx
#	dataset.idx2count
	
#	loader.kypt
	
#	DataTracker((dataset.train.inputs, dataset.train.targets), ~~~

class Dictionary(object):
	def __init__(self):
		self.word2count = {}
		self.word2idx = {} 
		self.idx2word = [] 
		self.idx2count = [] 

	def add_word(self, word):
		if word not in self.word2count:
			self.word2count[word]=1
		else:
			self.word2count[word]+=1 

	def build_vocab(self, min_freq=0):
		for word, count in self.word2count.items():
			if count>min_freq:
				self.idx2word.append(word)
				self.idx2count.append(count) 
				self.word2idx[word] = len(self.idx2word)-1

	def __len__(self): 
		return len(self.idx2word) 


class Loader(object): 
	def __init__(self, datatype):
		# datatype: ptb, kyp, kyt, kypt
		self.dictionary = Dictionary() 
		self.trainset = self.validset = None 


		if datatype=='kyp':
			self.load_kyp() 
		elif datatype=='kypt':
			self.load_kypt() 
		
	def load_ptb(self): 
		datapath = ['trainset', 'validset'] 
			


	def load_kyp(self):
		datapath = ['trainset', 'validset'] 
		
		for path in datapath: 
			file_list = os.listdir(os.path.join('ky', path))
			for name in file_list: 
				filename = os.path.join('ky', path, name) 
				with open(filename) as f:
					lists = f.readlines() 
					for line in lists: 
						each_line = line.rstrip().split(' ') 
						word = string.lower(each_line[2]).rstrip() 
						self.dictionary.add_word(word) 
		
		self.dictionary.build_vocab() 

		for path in datapath:
			sequences = [] 
			file_list = os.listdir(os.path.join('ky', path)) 
			for name in file_list:
				filename = os.path.join('ky', path, name) 
				sequence = []
				with open(filename) as f:
					lists = f.readlines() 
					for line in lists:
						each_line = line.rstrip().split(' ') 
						word = string.lower(each_line[2].rstrip())
						if word in self.dictionary.word2idx: 
							sequence.append(self.dictionary.word2idx[word]) 
				sequence = torch.LongTensor(sequence) 
				sequences.append(sequence) 
			setattr(self, path, sequences) 

	def load_kyt(self):
		pass
		
	def load_kypt(self): 
		datapath = ['trainset', 'validset'] 
	
	
		for path in datapath: 
			file_list = os.listdir(os.path.join('ky', path))
			for name in file_list: 
				filename = os.path.join('ky', path, name) 
				with open(filename) as f:
					lists = f.readlines() 
					for line in lists: 
						each_line = line.rstrip().split(' ') 
						word = string.lower(each_line[2]).rstrip() 
						self.dictionary.add_word(word) 
		
		self.dictionary.build_vocab() 

		mean = torch.zeros(len(self.dictionary), len(self.dictionary))
		mean2 = torch.zeros(len(self.dictionary), len(self.dictionary))
		count = torch.zeros(len(self.dictionary), len(self.dictionary)) 		

		for path in datapath: 
			prev_sequences, next_sequences, time_diffs = [], [], []
			file_list = os.listdir(os.path.join('ky', path)) 
			for name in file_list:
				filename = os.path.join('ky', path, name) 
				prev_sequence, next_sequence, time_diff = [], [], []
				with open(filename) as f:
					print(filename) 
					lists = f.readlines() 
					for i in range(len(lists)-1):

						def get_abs_time(date_str, date_type='<%Y%m%d %H:%M:%S:%f>'): 
							each_date = datetime.strptime(date_str, date_type) 
							return time.mktime(each_date.timetuple())+each_date.microsecond*1e-6
					
						prev_line = lists[i].rstrip().split(' ')
						prev_word = string.lower(prev_line[2].rstrip()) 
						prev_time = get_abs_time(string.join(prev_line[0:2]))

						next_line = lists[i+1].rstrip().split(' ')		
						next_word = string.lower(next_line[2].rstrip()) 
						next_time = get_abs_time(string.join(next_line[0:2]))

						prev_sequence.append(self.dictionary.word2idx[prev_word]) 
						next_sequence.append(self.dictionary.word2idx[next_word]) 
						
						diff = next_time-prev_time
						time_diff.append(diff)
		
#						if diff<6000: 
#							mean[self.dictionary.word2idx[prev_word], self.dictionary.word2idx[next_word]]+=(diff) 
#							mean2[self.dictionary.word2idx[prev_word], self.dictionary.word2idx[next_word]]+=(diff**2)
#							count[self.dictionary.word2idx[prev_word], self.dictionary.word2idx[next_word]]+=1

				prev_sequence = torch.LongTensor(prev_sequence) 
				next_sequence = torch.LongTensor(next_sequence) 
				
				time_diff = torch.Tensor(time_diff) 				

				if torch.max(time_diff)<6000:
					prev_sequences.append(prev_sequence) 
					next_sequences.append(next_sequence) 
					time_diffs.append(time_diff) 

					for p, n, t in zip(prev_sequence, next_sequence, time_diff):
						mean[p,n]+=t
						mean2[p,n]+=(t**2)
						count[p,n]+=1

			setattr(self, path, [prev_sequences, next_sequences, time_diffs]) 
		
		mean = mean/(count+1e-6)*(count!=0).float() 
		mean2/=(count+1e-6)*(count!=0).float()  
	
		std = (mean2-mean**2).sqrt() 
		
		self.mean = mean
		self.std = std
		self.count = count
