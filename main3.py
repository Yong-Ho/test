import torch
from torch import optim
import torch.nn as nn
from torch.autograd import Variable 
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import time 
import math 
import argparse
from data import Loader 

class PackedMSELoss(torch.nn.MSELoss):
	def __init__(self, ninp):
		super(PackedMSELoss, self).__init__() 
		self.loss = torch.nn.MSELoss() 
		self.ninp = ninp 

	def forward(self, outputs, length, targets):
		
		sort_length, sort_idx = length.sort(0, descending=True) 
		
		prev_targets = targets[0].index_select(1, sort_idx).long() 
		next_targets = targets[1].index_select(1, sort_idx).long() 
		time_targets = targets[2].index_select(1, sort_idx) 

		flatten_targets = (prev_targets*len(loader.dictionary)+next_targets)
		packed_targets = pack_padded_sequence(flatten_targets, list(sort_length.data))
		packed_time = pack_padded_sequence(time_targets, list(sort_length.data)) 
			
		outputs = outputs.gather(1, packed_targets[0].view(-1,1)).squeeze()

		return self.loss(outputs, packed_time[0]) 


class DataTracker(object): # for learning rnn packed sequence
	
	def __init__(self, datasets, batch_size, block_size, max_iter):

		# padding 
		datasets = datasets if type(datasets[0])==list else [datasets]

		max_length = 0 
		self.lengths = [] 
		for v in datasets[0]:
			max_length = max(len(v)-1, max_length) 
			self.lengths.append(len(v)-1)
		
		self.batch_size = batch_size
		self.block_size = block_size
		self.block_lengths = torch.Tensor(batch_size).cuda() 
		self.max_iter = max_iter 
		self.current_iter = 0 
		self.max_length = max_length 

		self.inputs = [] 
		self.targets = [] 

		# split input & target 
		for dataset in datasets: 
	
			inputs = torch.zeros(max_length, len(dataset)) 
			targets = torch.zeros(max_length, len(dataset)) 

			for i in range(len(dataset)):
				input = dataset[i][:-1]
				target = dataset[i][1:]
				length = input.size(0) 

				inputs[:length, i] = input
				targets[:length, i] = target
			
			assert(batch_size<=len(dataset)) 

			pad = torch.zeros(block_size-max_length%block_size, *inputs[0].size()) 

			# make inputs
			inputs = torch.cat([inputs, pad], 0)
			self.inputs.append(inputs.view(-1, block_size, *inputs[0].size()).cuda())	

			# make targets 
			targets = torch.cat([targets, pad], 0) 
			self.targets.append(targets.view(-1, block_size, *targets[0].size()).cuda())

		self.sample_iter = iter(torch.randperm(len(self.lengths)).long()) 
		self.indices = torch.LongTensor([next(self.sample_iter) for _ in range(batch_size)]).cuda() 
		self.last_step = torch.zeros(batch_size).long() 

	def __iter__(self):
		return self 
	
	def next(self): 

		if self.current_iter==self.max_iter:
			self.current_iter = 0 
			raise StopIteration
		
		input_blocks = []
		target_blocks = [] 
		
		for i in range(len(self.inputs)): 

			inputs = self.inputs[i].index_select(2, self.indices)
			targets = self.targets[i].index_select(2, self.indices)

			input_block = [] 
			target_block = []
			reset = [] 

			# change index
			for b in range(self.batch_size):
			
				input_block.append(inputs[self.last_step[b],:,b]) 
				target_block.append(targets[self.last_step[b],:,b]) 

				offset = self.lengths[self.indices[b]] - self.last_step[b]*self.block_size

				self.block_lengths[b] = min(offset, self.block_size)
				self.last_step[b]+=1

				if offset<=self.block_size:
					reset.append(b)
					try: 
						self.indices[b] = next(self.sample_iter)	
					except StopIteration:
						self.sample_iter = iter(torch.randperm(len(self.lengths)).long()) 
						self.indices[b] = next(self.sample_iter) 

					self.last_step[b] = 0 
			
			input_block = Variable(torch.stack(input_block, 1))
			target_block = Variable(torch.stack(target_block, 1)) 
			input_blocks.append(input_block) 
			target_blocks.append(target_block) 
	
		self.current_iter+=1 

		return input_blocks, target_blocks, Variable(self.block_lengths), reset 

# define model
class Model(nn.Module):
	def __init__(self, rnn_type, ninp, nhid, nfed):
		super(Model, self).__init__() 
		
		self.ninp = ninp 
		self.nhid = nhid 
		self.nfed = nfed 

		self.embedding = torch.nn.Embedding(ninp**2, nhid) 
		self.rnn = getattr(nn, rnn_type)(nhid, nhid, nfed) 
		self.rnn_type = rnn_type 
		self.decoding = torch.nn.Linear(nhid, ninp**2) 

#		self.decoding.weight = self.embedding.weight 

	def reset_parameters(self):
		stdv = 1.0/math.sqrt(self.nhid**2) 
		for weight in self.parameters():
			weight.data.uniform_(-stdv, stdv)

	def init_hidden(self, bsz):
		if self.rnn_type == 'LSTM':
			hidden = (Variable(torch.zeros(self.nfed, bsz, self.nhid).cuda()), 
				Variable(torch.zeros(self.nfed, bsz, self.nhid).cuda())) 
		else: 
			hidden = Variable(torch.zeros(self.nfed, bsz, self.nhid).cuda()) 
		return hidden 

	def forward(self, inputs, length, hidden=None):

		if hidden==None:
			hidden = self.init_hidden(inputs[0].size(1)) 

		sort_length, sort_idx = length.sort(0, descending=True) 
	
		prev_inputs = inputs[0].index_select(1, sort_idx)
		next_inputs = inputs[1].index_select(1, sort_idx)
		time_inputs = inputs[2].index_select(1, sort_idx) 

		concat = torch.cat([prev_inputs, next_inputs, time_inputs], 1) 

		prev_inputs = prev_inputs.long()
		next_inputs = next_inputs.long() 

		flatten_inputs = (prev_inputs*self.ninp+next_inputs) 		
		
		emb = self.embedding(flatten_inputs)
		scale = time_inputs.unsqueeze(2).expand_as(emb) 
		scaled_emb = scale*emb 

		if self.rnn_type == 'LSTM': 
			hidden = (hidden[0].index_select(1, sort_idx), hidden[1].index_select(1, sort_idx))
		else: 
			hidden = hidden.index_select(1, sort_idx) 
		
		packed = pack_padded_sequence(scaled_emb, list(sort_length.data))
		out, hidden = self.rnn(packed, hidden) 

		y = self.decoding(out[0]) 

		if self.rnn_type == 'LSTM': 
			hidden = (hidden[0].index_select(1, sort_idx), hidden[1].index_select(1, sort_idx))
		else:  
			hidden = hidden.index_select(1, sort_idx)  
			
		return y, hidden

if __name__ == '__main__': 

	parser = argparse.ArgumentParser(description='Argument Parser') 
	parser.add_argument('--model', type=str, default='LSTM') 
	parser.add_argument('--epochs', type=int, default=1000, help='upper epoch limit') 
	parser.add_argument('--iters', type=int, default=100, help='number of iteration per one epoch') 
	parser.add_argument('--nhid', type=int, default=200, help='number of hidden units per layer') 
	parser.add_argument('--nfed', type=int, default=2, help='number of layers') 
	parser.add_argument('--bsz', type=int, default=10, help='batch size') 
	parser.add_argument('--blk', type=int, default=150, help='sequence length per one iteration') 
	parser.add_argument('--lr', type=float, default=1e-3, help='initial learning rate') 
	parser.add_argument('--data', type=str, default='kyp') 	
	parser.add_argument('--seed', type=int, default=1111) 

	args = parser.parse_args() 
	
	torch.manual_seed(args.seed) 
	loader = Loader('kypt') 

	trainTracker = DataTracker(loader.trainset, args.bsz, args.blk, args.iters) 
	validTracker = DataTracker(loader.validset, args.bsz, args.blk, args.iters) 

	model = Model(args.model, len(loader.dictionary), args.nhid, args.nfed).cuda()
	model.reset_parameters() 

	criterion = PackedMSELoss(len(loader.dictionary)).cuda() 

	optimizer = optim.SGD(model.parameters(), lr=args.lr) 

	train_hidden = valid_hidden = None

	def repackage_hidden(h, reset=None):
		if type(h)==Variable:
			if reset is not None: 
				for r in reset: 
					h.data[:,r,:]=0 

			return Variable(h.data) 

		else:
			new_h = [] 
			for v in h:
				if reset is not None:
					for r in reset: 
						v.data[:,r,:]=0

				new_h.append(Variable(v.data)) 
			return tuple(new_h)

	for epoch in range(args.epochs): 
	
		start_time = time.time() 

		# training
		train_error = train_count = 0
		model.train() 
		for inputs, targets, length, reset in trainTracker:

			optimizer.zero_grad() 

			idx = (inputs[0].view(-1)*len(loader.dictionary)+inputs[1].view(-1)).data.long().cpu() 
			mean = loader.mean.view(-1)
			std = loader.std.view(-1) 
			input_mean = mean[idx].view(inputs[2].size()) 
			input_std = std[idx].view(inputs[2].size())
			
			inputs[2] = Variable(inputs[2].data-input_mean.cuda()/(input_std.cuda()+1e-6)*(input_std!=0).float().cuda())
	
			out, train_hidden = model(inputs, length, train_hidden)
			idx = (targets[0].view(-1)*len(loader.dictionary)+targets[1].view(-1)).data.long().cpu() 
			target_mean = mean[idx].view(targets[2].size())
			target_std = std[idx].view(targets[2].size())
			targets[2] = Variable(targets[2].data-target_mean.cuda()/(target_std.cuda()+1e-6)*(target_std!=0).float().cuda())

			loss = criterion(out, length, targets) 
			train_count += len(out) 
			train_error += len(out)*loss.data[0] 
			loss.backward()
			optimizer.step() 
			train_hidden = repackage_hidden(train_hidden, reset) 

		# validation
		valid_error = valid_count = 0
		model.eval() 
		for inputs, targets, length, reset in validTracker:
			out, valid_hidden = model(inputs, length, valid_hidden) 
			loss = criterion(out, length, targets) 
			valid_count += len(out) 
			valid_error += len(out)*loss.data[0] 

			valid_hidden = repackage_hidden(valid_hidden, reset) 
		
		elapsed_time = time.time()-start_time
		remaining_time = time.strftime('%H:%M:%S', time.gmtime(elapsed_time*(args.epochs-epoch)))
		
		train_error = train_error/float(train_count) 
		valid_error = valid_error/float(valid_count) 
		print('| epoch {:4d} | train_ppl {:8.4f} | valid_ppl {:8.4f} | remaning '.format(epoch, train_error, valid_error) + remaining_time + ' |') 
		
		optimizer.param_groups[0]['lr']/=1.05
